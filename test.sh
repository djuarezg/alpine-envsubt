#!/bin/bash

expected='--name=gitlab'
actual=$(docker run -e "name=gitlab" alpine-envsubst '--name=${name}')

if [[ $actual != $expected ]]; then
  echo "expected '$expected', got '$actual'"
  exit 1
fi

exit 0
